MaharajaDAO Exchange
====================

[![Build Status](https://travis-ci.com/makerdao/oasis-landing.svg?token=ypax3WLsfk1Dj4t78DCV&branch=master)](https://travis-ci.com/makerdao/oasis-landing)

MaharajaDEX is a fork of OasisDEX by MakerDAO. It allows you to trade MAHA/Laxmicoin tokens with ETH.

## Development

Start the development server:

`yarn develop`

Build:

`yarn build`

Update the staging website:

`now --prod`

(You can install "now" with `yarn global add now`)
